/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import graphs.Graph;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import metro.Percurso;
import metro.Station;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author TiagoRibeiro(1170426
 */
public class CalculoPercursosTest {
    
    public Graph<Station, String> graph = new Graph(true);

    Station st1 = new Station("st1", "1");
    Station st2 = new Station("st2", "1");
    Station st3 = new Station("st2", "2");
    Station st4 = new Station("st3", "2");
    Station st5 = new Station("st4", "2");
    Station st6 = new Station("st4", "3");
    Station st7 = new Station("st4", "1");
    Station st8 = new Station("st5", "2");
    
    public CalculoPercursosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        graph.insertVertex(st1);
        graph.insertVertex(st2);
        graph.insertVertex(st3);
        graph.insertVertex(st4);
        graph.insertVertex(st5);
        graph.insertVertex(st6);
        graph.insertVertex(st7);

        graph.insertEdge(st1, st2, "1", 1);
        graph.insertEdge(st2, st3, "SS", 0);
        graph.insertEdge(st3, st4, "2", 4);
        graph.insertEdge(st4, st5, "2", 3);
        graph.insertEdge(st5, st6, "SS", 0);
        graph.insertEdge(st6, st7, "SS", 0);
        graph.insertEdge(st7, st1, "1", 93);
        graph.insertEdge(st3, st5, "2", 75);
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of percursoMaisCurtoEstacoes method, of class CalculoPercursos.
     */
    @Test
    public void testPercursoMaisCurtoEstacoes() {
        System.out.println("percursoMaisCurtoEstacoes");
        LocalTime instanteInicial = LocalTime.MIN;
        Percurso expResult = new Percurso(LocalTime.MIN);
        expResult.addStation(st3, 0);
        expResult.addStation(st5, 75);
        Percurso result = CalculoPercursos.percursoMaisCurtoEstacoes(graph, st3, st5, instanteInicial);
        System.out.println(expResult);
        System.out.println(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of percursoMaisCurtoTempo method, of class CalculoPercursos.
     */
    @Test
    public void testPercursoMaisCurtoTempo() {
        System.out.println("percursoMaisCurtoTempo");
        LocalTime instanteInicial = LocalTime.MIN;
        Percurso expResult = new Percurso(LocalTime.MIN);
        expResult.addStation(st3, 0);
        expResult.addStation(st4, 4);
        expResult.addStation(st5, 3);
        Percurso result = CalculoPercursos.percursoMaisCurtoTempo(graph, st3, st5, instanteInicial);
        assertEquals(expResult, result);
    }

    /**
     * Test of percursoMaisCurtoLinhas method, of class CalculoPercursos.
     */
    @Test
    public void testPercursoMaisCurtoLinhas() {
        System.out.println("percursoMaisCurtoLinhas");
        LocalTime instanteInicial = LocalTime.MIN;
        Percurso expResult = new Percurso(LocalTime.MIN);
        graph.insertEdge(st1, st7, "1", 93);
        expResult.addStation(st1, 0);
        expResult.addStation(st7, 93);
        Percurso result = CalculoPercursos.percursoMaisCurtoLinhas(graph, st1, st7, instanteInicial);
        System.out.println(expResult);
        System.out.println(result);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of percursoMaisCurtoLinhas method, of class CalculoPercursos.
     */
    @Test
    public void testPercursoMaisCurtoIntermedios() {
        
        graph.insertVertex(st8);
        
        graph.insertEdge(st3, st8, "2", 90);
        graph.insertEdge(st8, st4, "2", 54);
        
        System.out.println("percursoMaisCurtoLinhas");
        LocalTime instanteInicial = LocalTime.MIN;
        Percurso expResult = new Percurso(LocalTime.MIN);
        
        List<Station> intermedios = new ArrayList<>();
        intermedios.add(st2);
        intermedios.add(st4);
        intermedios.add(st8);
        
        expResult.addStation(st1, 0);
        expResult.addStation(st2,1);
        expResult.addStation(st3, 0);
        expResult.addStation(st8, 90);
        expResult.addStation(st4, 54);
        expResult.addStation(st5, 3);
        expResult.addStation(st6, 0);
        expResult.addStation(st7, 0);
        
        Percurso result = CalculoPercursos.percursoMaisCurtoIntermedios(graph, st1, st7, intermedios, instanteInicial);
        
        assertEquals(expResult, result);
    }
    
}
