/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import graphs.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import metro.Station;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author TiagoRibeiro(1170426
 */
public class ConectividadeTest {

    public Graph<Station, String> graph = new Graph(true);

    Station st1 = new Station("st1", "1");
    Station st2 = new Station("st2", "1");
    Station st3 = new Station("st2", "2");
    Station st4 = new Station("st3", "2");
    Station st5 = new Station("st4", "2");
    Station st6 = new Station("st4", "3");
    Station st7 = new Station("st4", "1");

    public ConectividadeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        graph.insertVertex(st1);
        graph.insertVertex(st2);
        graph.insertVertex(st3);
        graph.insertVertex(st4);
        graph.insertVertex(st5);
        graph.insertVertex(st6);
        graph.insertVertex(st7);

        graph.insertEdge(st1, st2, "1", 1);
        graph.insertEdge(st2, st3, "SS", 0);
        graph.insertEdge(st3, st4, "2", 4);
        graph.insertEdge(st4, st5, "2", 3);
        graph.insertEdge(st5, st6, "SS", 0);
        graph.insertEdge(st6, st7, "SS", 0);

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of isConexo method, of class Conectividade.
     */
    @Test
    public void testIsConexoFalse() {

        graph.removeEdge(st6, st7);
        System.out.println("isConexo");
        boolean expResult = false;
        boolean result = Conectividade.isConexo(graph);
        assertEquals(expResult, result);
    }

    @Test
    public void testIsConexoTrue() {
        System.out.println("isConexo");
        boolean expResult = true;
        boolean result = Conectividade.isConexo(graph);
        assertEquals(expResult, result);
    }

    /**
     * Test of getComponents method, of class Conectividade.
     */
    @Test
    public void testGetComponents() {
        System.out.println("getComponents");
        ArrayList<Station> vertices = new ArrayList<>(Arrays.asList(graph.allkeyVerts()));
        ArrayList<LinkedList<Station>> expResult = new ArrayList<>();
        LinkedList<Station> vertices2 = new LinkedList<>();
        vertices2.addAll(vertices);
        expResult.add(vertices2);
        ArrayList<LinkedList<Station>> result = Conectividade.getComponents(graph, vertices);
        assertEquals(expResult, result);
    }

}
