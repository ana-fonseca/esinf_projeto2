/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import graphs.Graph;
import java.util.LinkedList;
import metro.Station;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author TiagoRibeiro(1170426
 */
public class AuxiliaresTest {
    
    public Graph<Station, String> graph = new Graph(false);

    Station st1 = new Station("st1", "1");
    Station st2 = new Station("st2", "1");
    Station st3 = new Station("st2", "2");
    Station st4 = new Station("st3", "2");
    Station st5 = new Station("st4", "2");
    Station st6 = new Station("st4", "3");
    Station st7 = new Station("st4", "1");
    Station st8 = new Station("st5", "2");
    
    public AuxiliaresTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        graph.insertVertex(st1);
        graph.insertVertex(st2);
        graph.insertVertex(st3);
        graph.insertVertex(st4);
        graph.insertVertex(st5);
        graph.insertVertex(st6);
        graph.insertVertex(st7);

        graph.insertEdge(st1, st2, "1", 1);
        graph.insertEdge(st2, st3, "SS", 0);
        graph.insertEdge(st3, st4, "2", 4);
        graph.insertEdge(st4, st5, "2", 3);
        graph.insertEdge(st5, st6, "SS", 0);
        graph.insertEdge(st6, st7, "SS", 0);
        graph.insertEdge(st7, st1, "1", 93);
        graph.insertEdge(st3, st5, "2", 75);
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of shortestPath method, of class Auxiliares.
     */
    @Test
    public void testShortestPathLinhas() {
        System.out.println("shortestPath");
        LinkedList<Station> shortPath = new LinkedList<>();
        double expResult = 999;
        LinkedList<Station> expected = new LinkedList<>();
        expected.add(st1);
        expected.add(st7);
        double result = Auxiliares.shortestPath(graph, st1, st7, shortPath, 1);
        assertEquals(expResult, result, 999);
    }
    
    @Test
    public void testShortestPathStations() {
        System.out.println("shortestPath");
        LinkedList<Station> shortPath = new LinkedList<>();
        double expResult = 1;
        LinkedList<Station> expected = new LinkedList<>();
        expected.add(st3);
        expected.add(st5);
        double result = Auxiliares.shortestPath(graph, st3, st5, shortPath, 2);
        assertEquals(expResult, result, 1);
        assertEquals(shortPath, expected);
    }
    
}
