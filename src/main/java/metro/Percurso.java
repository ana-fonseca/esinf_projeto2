package metro;

import graphs.Vertex;
import java.time.LocalTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeSet;

public class Percurso {

    private LocalTime instanteInicial = LocalTime.MIN;
    private LocalTime instante;
    private TreeSet<String> linhas = new TreeSet<>();
    private Map<Station, LocalTime> percurso = new LinkedHashMap<>();
    private int size = 0;

    public Percurso() {
        instanteInicial = LocalTime.MIN;
        instante = instanteInicial;
    }

    public Percurso(LocalTime instanteInicial) {
        this.instanteInicial = instanteInicial;
        instante = this.instanteInicial;
    }

    public Percurso(int horas, int minutos, int segundos) {
        instanteInicial = instanteInicial.withHour(horas);
        instanteInicial = instanteInicial.withMinute(minutos);
        instanteInicial = instanteInicial.withSecond(segundos);
        instanteInicial = instanteInicial.withNano(0);
        instante = this.instanteInicial;
    }

    public Percurso(int horas, int minutos) {
        instanteInicial = instanteInicial.withHour(horas);
        instanteInicial = instanteInicial.withMinute(minutos);
        instanteInicial = instanteInicial.withSecond(0);
        instanteInicial = instanteInicial.withNano(0);
        instante = this.instanteInicial;
    }

    public Percurso(int horas) {
        instanteInicial = instanteInicial.withHour(horas);
        instanteInicial = instanteInicial.withMinute(0);
        instanteInicial = instanteInicial.withSecond(0);
        instanteInicial = instanteInicial.withNano(0);
        instante = this.instanteInicial;
    }

    private void addLinhas(String linha) {
        linhas.add(linha);
    }

    public void addStation(Station vert, double weight) {
        instante = instante.plusMinutes(Math.round(weight));
        if (!countRepeated(vert)) {
            size++;
        }
        addLinhas(vert.getLineCode());
        percurso.put(vert, instante);
    }

    private boolean countRepeated(Station vert) {
        for (Station v : percurso.keySet()) {
            if (vert.getName().equals(v.getName())) {
                return true;
            }
        }
        return false;
    }

    public void setLinhas(TreeSet<String> linhas) {
        this.linhas = linhas;
    }

    public void setPercurso(Map<Station, LocalTime> percurso) {
        this.percurso = percurso;
    }

    @Override
    public String toString() {
        String str = "Número de estações pelas quais passa: " + size
                + "\nLinhas: " + linhas;
        for (Station vertex : percurso.keySet()) {
            str += "\n" + vertex + ", " + percurso.get(vertex);
        }
        return str;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Percurso)) {
            return false;
        }
        
        Percurso otherP = (Percurso) other;
        
        if (!instanteInicial.equals(otherP.instanteInicial)){
            return false;
        }
        
        if (!linhas.equals(otherP.linhas)){
            return false;
        }
        
        if (!percurso.equals(otherP.percurso)){
            return false;
        }
        
        if (size != otherP.size){
            return false;
        }
        
        return true;
    }

}
