package metro;

import java.util.Objects;

public class Station {
    
    private String lineCode;
    private String name;
    private float longitude;
    private float latitude;
    
    public Station(String name, String lineCode) {
        this.lineCode = lineCode;
        this.name = name;
    }

    public String getLineCode() {
        return lineCode;
    }

    public String getName() {
        return name;
    }

    public float getLongitude() {
        return longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLineCode(String lineCode) {
        this.lineCode = lineCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCoordinates(float longitude, float latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return name + ", " + lineCode + " - " + "(" + longitude + ", " + latitude + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Station other = (Station) obj;
        if (Float.floatToIntBits(this.longitude) != Float.floatToIntBits(other.longitude)) {
            return false;
        }
        if (Float.floatToIntBits(this.latitude) != Float.floatToIntBits(other.latitude)) {
            return false;
        }
        if (!Objects.equals(this.lineCode, other.lineCode)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
}
