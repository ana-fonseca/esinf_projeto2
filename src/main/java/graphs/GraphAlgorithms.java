/*
* A collection of graph algorithms.
 */
package graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF
 */
public class GraphAlgorithms {

    /**
     * Performs breadth-first search of a Graph starting in a Vertex
     *
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qbfs a queue with the vertices of breadth-first search
     */
    public static <V, E> LinkedList<V> BreadthFirstSearch(Graph<V, E> g, V vert) {

        if (!g.validVert(vert)) {
            return null;
        }

        LinkedList<V> qbfs = new LinkedList<>();
        LinkedList<V> qaux = new LinkedList<>();
        boolean[] visited = new boolean[g.numVertices()];  //default initializ.: false

        qbfs.add(vert);
        qaux.add(vert);
        int vKey = g.getKey(vert);
        visited[vKey] = true;

        while (!qaux.isEmpty()) {
            vert = qaux.remove();
            for (Edge<V, E> edge : g.outgoingEdges(vert)) {
                V vAdj = g.opposite(vert, edge);
                vKey = g.getKey(vAdj);
                if (!visited[vKey]) {
                    qbfs.add(vAdj);
                    qaux.add(vAdj);
                    visited[vKey] = true;
                }
            }
        }
        return qbfs;
    }

    /**
     * Performs depth-first search starting in a Vertex
     *
     * @param g Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param visited set of discovered vertices
     * @param qdfs queue with vertices of depth-first search
     */
    private static <V, E> void DepthFirstSearch(Graph<V, E> g, V vOrig, boolean[] visited, LinkedList<V> qdfs) {

        visited[g.getKey(vOrig)] = true;
        qdfs.add(vOrig);

        for (V vertAdj : g.adjVertices(vOrig)) {
            if (!visited[g.getKey(vertAdj)]) {
                DepthFirstSearch(g, vertAdj, visited, qdfs);
            }
        }

    }

    /**
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static <V, E> LinkedList<V> DepthFirstSearch(Graph<V, E> g, V vert) {

        if (!g.validVert(vert)) {
            return null;
        }

        boolean[] visited = new boolean[g.numVertices()];
        LinkedList<V> qdfs = new LinkedList<>();

        DepthFirstSearch(g, vert, visited, qdfs);

        return qdfs;
    }

    /**
     * Returns all paths from vOrig to vDest
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path stack with vertices of the current path (the path is in
     * reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    private static <V, E> void allPaths(Graph<V, E> g, V vOrig, V vDest, boolean[] visited,
            LinkedList<V> path, ArrayList<LinkedList<V>> paths) {

        path.push(vOrig);
        int originIndex = g.getKey(vOrig);
        visited[originIndex] = true;

        for (V vertice : g.adjVertices(vOrig)) {
            if (vertice.equals(vDest)) {
                path.push(vertice);
                paths.add(path);
                path.pop();
            } else if (!visited[g.getKey(vertice)]) {
                allPaths(g, vertice, vDest, visited, path, paths);
            }
        }

        visited[originIndex] = false;
        path.pop();
    }

    /**
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <V, E> ArrayList<LinkedList<V>> allPaths(Graph<V, E> g, V vOrig, V vDest) {

        if (!g.validVert(vOrig)) {
            return null;
        }

        if (!g.validVert(vDest)) {
            return null;
        }

        boolean[] visited = new boolean[g.numVertices()];
        LinkedList<V> path = new LinkedList<>();
        ArrayList<LinkedList<V>> allPaths = new ArrayList<>();

        allPaths(g, vOrig, vDest, visited, path, allPaths);

        return allPaths;
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights This implementation
     * uses Dijkstra's algorithm
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param visited set of discovered vertices
     * @param pathkeys minimum path vertices keys
     * @param dist minimum distances
     */
    private static <V, E> void shortestPathLength(Graph<V, E> g, V vOrig, V[] vertices,
            boolean[] visited, int[] pathKeys, double[] dist) {

        for (V vertice : vertices) {
            int index = g.getKey(vertice);
            pathKeys[index] = -1;
            dist[index] = Double.MAX_VALUE;
            visited[index] = false;
        }
        
        dist[g.getKey(vOrig)] = 0;

        while (vOrig != null) {
            int indexVertOrig = g.getKey(vOrig);
            visited[indexVertOrig] = true;

            for (V adjVertice : g.adjVertices(vOrig)) {
                Edge edge = g.getEdge(vOrig, adjVertice);
                int indexVertAdj = g.getKey(adjVertice);
                if (!visited[indexVertAdj] && dist[indexVertAdj] > dist[indexVertOrig] + edge.getWeight()) {
                    dist[indexVertAdj] = dist[indexVertOrig] + edge.getWeight();
                    pathKeys[indexVertAdj] = indexVertOrig;
                }
            }

            double minDist = Double.MAX_VALUE;

            vOrig = null;

            for (V vertice : vertices) {
                int index = g.getKey(vertice);
                if (dist[index] < minDist && !visited[index]) {
                    minDist = dist[index];
                    vOrig = vertice;
                }
            }
        }
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf The path
     * is constructed from the end to the beginning
     *
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @param pathkeys minimum path vertices keys
     * @param path stack with the minimum path (correct order)
     */
    public static <V, E> void getPath(Graph<V, E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path) {

        path.add(vDest);
        if (!vOrig.equals(vDest)) {
            vDest = verts[pathKeys[g.getKey(vDest)]];
            getPath(g, vOrig, vDest, verts, pathKeys, path);
        }
    }

    //shortest-path between voInf and vdInf
    public static <V, E> double shortestPath(Graph<V, E> g, V vOrig, V vDest, LinkedList<V> shortPath) {

        if (!g.validVert(vOrig) || !g.validVert(vDest)) { //If either of the vertices are invalid, return -1
            return -1;
        }

        int numVertices = g.numVertices();
            
        V[] vertices = g.allkeyVerts();
        boolean[] visited = new boolean[numVertices];
        int[] pathKeys = new int[numVertices];
        double[] dist = new double[numVertices];

        shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);

        int destIndex = g.getKey(vDest);

        if (!visited[destIndex]) { //If we didn't get to destiny
            return -1;
        }

        getPath(g, vOrig, vDest, vertices, pathKeys, shortPath);

        Collections.reverse(shortPath); //Reverse path

        return dist[destIndex];
    }

    /**
     * Reverses the path
     *
     * @param path stack with path
     */
    private static <V, E> LinkedList<V> revPath(LinkedList<V> path) {

        LinkedList<V> pathcopy = new LinkedList<>(path);
        LinkedList<V> pathrev = new LinkedList<>();

        while (!pathcopy.isEmpty()) {
            pathrev.push(pathcopy.pop());
        }

        return pathrev;
    }
}