package utils;

import graphs.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import metro.Station;

public class FileManager<V, E> {

    private static final String COORDINATES = "coordinates.csv";
    private static final String CONNECTIONS = "connections.csv";
    private static final String LINES_STATIONS = "lines_and_stations.csv";
    private static final String DIVIDER = ";";

    private static Graph<Station, String> graph = new Graph<>(true);

    public static boolean readFile(File file) throws Exception {
        boolean worked = false;
        if (file.exists()) {
            List<String> list = Files.lines(Paths.get(file.getAbsolutePath())).collect(Collectors.toList());
            switch (file.getName()) {
                case COORDINATES:
                    worked = readCoordinates(list);
                    break;
                case CONNECTIONS:
                    worked = readConnections(list);
                    break;
                case LINES_STATIONS:
                    worked = readLinesStations(list);
                    break;
                default:
                    break;
            }
        }
        if (!worked) {
            graph = new Graph<>(true);
        }
        return worked;
    }

    private static <V, E> boolean readCoordinates(List<String> l) {
        float latitude;
        float longitude;
        for (String str : l) {
            String[] line = str.trim().split(DIVIDER);
            if (line.length == 3) {
                String stationName = line[0];
                try {
                    latitude = Float.parseFloat(line[1]);
                    longitude = Float.parseFloat(line[2]);
                    if (!stationName.isEmpty()) {
                        for (Station vertex : graph.vertices()) {
                            if (vertex.getName().equals(stationName)) {
                                vertex.setCoordinates(latitude, longitude);
                            }
                        }
                    }
                } catch (Exception e) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    private static boolean readConnections(List<String> l) throws Exception {
        for (String str : l) {
            String[] line = str.trim().split(DIVIDER);
            if (line.length == 4) {
                String lineCode = line[0];
                String stationNameOrigin = line[1];
                String stationNameDestination = line[2];
                try {
                    int time = Integer.parseInt(line[3]);
                    if (time >= 0 && !lineCode.isEmpty() && !stationNameOrigin.isEmpty() && !stationNameDestination.isEmpty()) {
                        Station stationOrigin = null;
                        Station stationDestination = null;
                        for (Station v : graph.vertices()) {
                            if (v.getLineCode().equals(lineCode)) {
                                if (v.getName().equals(stationNameOrigin)) {
                                    stationOrigin = v;
                                }
                                if (v.getName().equals(stationNameDestination)) {
                                    stationDestination = v;
                                }
                            }
                        }
                        if (stationOrigin != null && stationDestination != null) {
                            graph.insertEdge(stationOrigin, stationDestination, lineCode, time);
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } catch (Exception e) {
                    return false;
                }
            } else {
                return false;
            }
        }
        sameStationConnection();
        return true;
    }

    private static void sameStationConnection() {
        for (Station v1 : graph.vertices()) {
            for (Station v2 : graph.vertices()) {
                if (v1.getName().equals(v2.getName()) && !v1.getLineCode().equals(v2.getLineCode())) {
                    graph.insertEdge(v1, v2, "SS", 0); //SS = Same Station
                    graph.insertEdge(v2, v1, "SS", 0); //SS = Same Station
                }
            }
        }
    }

    private static boolean readLinesStations(List<String> l) {
        for (String str : l) {
            String[] line = str.trim().split(DIVIDER);
            if (line.length == 2) {
                String lineCode = line[0];
                String stationName = line[1];
                if (!lineCode.isEmpty() && !stationName.isEmpty()) {
                    Station station = new Station(stationName, lineCode);
                    graph.insertVertex(station);
                } else {
                    return false;
                }
            } else {
                return false;
            }

        }
        return true;
    }

    public static Graph<Station, String> getGraph() {
        return graph;
    }

    public static void setGraph(Graph graph) {
        FileManager.graph = graph;
    }
}
