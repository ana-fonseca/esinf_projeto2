package utils;

import graphs.Graph;
import graphs.GraphAlgorithms;
;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import metro.Percurso;
import metro.Station;



public class CalculoPercursos {

    public static Percurso percursoMaisCurtoEstacoes(Graph<Station, String> graph, Station vOrig, Station vDest, LocalTime instanteInicial) {
        LinkedList<Station> list = new LinkedList<>();
        double num = Auxiliares.shortestPath(graph, vOrig, vDest, list, 2);
        if (num == -1) {
            return null;
        }
        return createPath(graph, list, instanteInicial);
    }

    public static Percurso percursoMaisCurtoTempo(Graph graph, Station vOrig, Station vDest, LocalTime instanteInicial) {
        LinkedList<Station> list = new LinkedList<>();
        double num = GraphAlgorithms.shortestPath(graph, vOrig, vDest, list);
        if (num == -1) {
            return null;
        }
        return createPath(graph, list, instanteInicial);
    }

    public static Percurso percursoMaisCurtoLinhas(Graph graph, Station vOrig, Station vDest, LocalTime instanteInicial) {
        LinkedList<Station> list = new LinkedList<>();
        double num = Auxiliares.shortestPath(graph, vOrig, vDest, list, 1);
        if (num == -1) {
            return null;
        }
        return createPath(graph, list, instanteInicial);
    }

    public static Percurso percursoMaisCurtoIntermedios(Graph graph, Station vOrig, Station vDest, List<Station> stations, LocalTime instanteInicial) {
        LinkedList<LinkedList<Station>> caminhos = new LinkedList<>();
        LinkedList<Station> tmp = new LinkedList<>();
        todasCombinacoes(caminhos, stations, tmp);
        int menorTempo = Integer.MAX_VALUE;
        LinkedList<Station> menorCaminho = new LinkedList<>();
        for (LinkedList<Station> list : caminhos) {
            LinkedList<Station> finalPath = new LinkedList<>();
            int tmpTime = menorTempoList(graph, list, finalPath, vOrig, vDest);
            if (tmpTime < menorTempo) {
                menorTempo = tmpTime;
                menorCaminho = new LinkedList<>(finalPath);
            }
        }
        return createPath(graph, menorCaminho, instanteInicial);
    }

    private static Percurso createPath(Graph graph, LinkedList<Station> path, LocalTime instanteInicial) {
        Percurso perc = new Percurso(instanteInicial);
        for (int i = 0; i < path.size(); i++) {
            if (i == 0) {
                perc.addStation(path.get(i), 0);
            } else {
                perc.addStation(path.get(i), graph.getEdge(path.get(i - 1), path.get(i)).getWeight());
            }
        }
        return perc;
    }

    private static void todasCombinacoes(LinkedList<LinkedList<Station>> caminhos, List<Station> stations, LinkedList<Station> tmp) {
        List<Station> tmpStations = new ArrayList<>(stations);
        if (stations.isEmpty()) {
            caminhos.add(new LinkedList<>(tmp));
            tmp.removeLast();
        } else {
            for (Station atual : stations) {
                tmp.add(atual);
                tmpStations.remove(atual);
                todasCombinacoes(caminhos, tmpStations, tmp);
                tmpStations.add(atual);
                tmp.remove(atual);
            }
        }
    }

    private static int menorTempoList(Graph graph, LinkedList<Station> list, LinkedList<Station> caminho, Station vOrig, Station vDest) {
        ListIterator atual = list.listIterator();
        LinkedList<Station> tmp = new LinkedList<>();
        Station station = (Station) atual.next();
        int tempoAtual = (int) Math.round(GraphAlgorithms.shortestPath(graph, vOrig, station, tmp));
        caminho.addAll(tmp);
        if (list.size() == 1) {
            tmp.clear();
            tempoAtual += (int) Math.round(GraphAlgorithms.shortestPath(graph, station, vDest, tmp));
            tmp.removeFirst();
            caminho.addAll(tmp);
        } else {
            atual.previous();
            while (atual.hasNext()){
                Station stationInicio = (Station) atual.next();
                tmp.clear();
                if (atual.hasNext()){
                    Station stationFim = (Station) atual.next();
                    atual.previous();
                    tempoAtual += (int) Math.round(GraphAlgorithms.shortestPath(graph, stationInicio, stationFim, tmp));
                    tmp.removeFirst();
                    caminho.addAll(tmp);
                } else {
                    tempoAtual += (int) Math.round(GraphAlgorithms.shortestPath(graph, stationInicio, vDest, tmp));
                    tmp.removeFirst();
                    caminho.addAll(tmp);
                }
            }
        }
        return tempoAtual;
    }

}
