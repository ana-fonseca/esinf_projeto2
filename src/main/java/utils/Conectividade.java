package utils;

import graphs.Graph;
import static graphs.GraphAlgorithms.BreadthFirstSearch;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class Conectividade {
    
    public static <Station, String> boolean isConexo(Graph<Station, String> graph) {
        ArrayList<Station> vertices = new ArrayList<>(Arrays.asList(graph.allkeyVerts()));
        return getComponents(graph, vertices).size() <=  1;
    }

    public static <Station, String> ArrayList<LinkedList<Station>> getComponents(Graph<Station, String> graph, ArrayList<Station> vertices) {
        ArrayList<LinkedList<Station>> components = new ArrayList<>();
        LinkedList<Station> check = new LinkedList<>();
        Set<Station> tmp = new HashSet<>(vertices);
        for (int i = 0;i < vertices.size();i++) {
            if (tmp.contains(vertices.get(i))) {
                check = BreadthFirstSearch(graph, vertices.get(i));
                components.add(check);
            }
            tmp.removeAll(check);
        }
        return components;
    }
    
}
