package utils;

import graphs.Edge;
import graphs.Graph;
import graphs.GraphAlgorithms;
import java.util.Collections;
import java.util.LinkedList;
import metro.Station;

public class Auxiliares {

    private static void shortestPathLinhas(Graph<Station, String> g, Station vOrig, Station[] vertices,
            boolean[] visited, int[] pathKeys, double[] dist) {

        for (Station vertice : vertices) {
            int index = g.getKey(vertice);
            pathKeys[index] = -1;
            dist[index] = Double.MAX_VALUE;
            visited[index] = false;
        }

        dist[g.getKey(vOrig)] = 0;

        while (vOrig != null) {
            int indexVertOrig = g.getKey(vOrig);
            visited[indexVertOrig] = true;

            for (Station adjVertice : g.adjVertices(vOrig)) {
                Edge edge = g.getEdge(vOrig, adjVertice);
                int indexVertAdj = g.getKey(adjVertice);
                if (!edge.getElement().equals("SS")) {
                    if (!visited[indexVertAdj] && dist[indexVertAdj] > dist[indexVertOrig] + 999) {
                        dist[indexVertAdj] = dist[indexVertOrig] + 999;
                        pathKeys[indexVertAdj] = indexVertOrig;
                    }
                } else {
                    if (!visited[indexVertAdj] && dist[indexVertAdj] > dist[indexVertOrig] + edge.getWeight()) {
                        dist[indexVertAdj] = dist[indexVertOrig] + edge.getWeight();
                        pathKeys[indexVertAdj] = indexVertOrig;
                    }
                }
            }

            double minDist = Double.MAX_VALUE;

            vOrig = null;

            for (Station vertice : vertices) {
                int index = g.getKey(vertice);
                if (dist[index] < minDist && !visited[index]) {
                    minDist = dist[index];
                    vOrig = vertice;
                }
            }
        }
    }
    
    private static void shortestPathStations(Graph<Station, String> g, Station vOrig, Station[] vertices,
            boolean[] visited, int[] pathKeys, double[] dist) {

        for (Station vertice : vertices) {
            int index = g.getKey(vertice);
            pathKeys[index] = -1;
            dist[index] = Double.MAX_VALUE;
            visited[index] = false;
        }

        dist[g.getKey(vOrig)] = 0;

        while (vOrig != null) {
            int indexVertOrig = g.getKey(vOrig);
            visited[indexVertOrig] = true;

            for (Station adjVertice : g.adjVertices(vOrig)) {
                Edge edge = g.getEdge(vOrig, adjVertice);
                int indexVertAdj = g.getKey(adjVertice);
                if (edge.getElement().equals("SS")) {
                    if (!visited[indexVertAdj] && dist[indexVertAdj] > dist[indexVertOrig] + 0) {
                        dist[indexVertAdj] = dist[indexVertOrig] + 0;
                        pathKeys[indexVertAdj] = indexVertOrig;
                    }
                } else {
                    if (!visited[indexVertAdj] && dist[indexVertAdj] > dist[indexVertOrig] + 1) {
                        dist[indexVertAdj] = dist[indexVertOrig] + 1;
                        pathKeys[indexVertAdj] = indexVertOrig;
                    }
                }
            }

            double minDist = Double.MAX_VALUE;

            vOrig = null;

            for (Station vertice : vertices) {
                int index = g.getKey(vertice);
                if (dist[index] < minDist && !visited[index]) {
                    minDist = dist[index];
                    vOrig = vertice;
                }
            }
        }
    }

    //shortest-path between voInf and vdInf
    public static double shortestPath(Graph<Station, String> g, Station vOrig, Station vDest, LinkedList<Station> shortPath, int option) {

        if (!g.validVert(vOrig) || !g.validVert(vDest)) { //If either of the vertices are invalid, return -1
            return -1;
        }

        int numVertices = g.numVertices();

        Station[] vertices = g.allkeyVerts();
        boolean[] visited = new boolean[numVertices];
        int[] pathKeys = new int[numVertices];
        double[] dist = new double[numVertices];

        if (option == 1){
            shortestPathLinhas(g, vOrig, vertices, visited, pathKeys, dist);
        } else {
            shortestPathStations(g, vOrig, vertices, visited, pathKeys, dist);
        }

        int destIndex = g.getKey(vDest);

        if (!visited[destIndex]) { //If we didn't get to destiny
            return -1;
        }

        GraphAlgorithms.getPath(g, vOrig, vDest, vertices, pathKeys, shortPath);

        Collections.reverse(shortPath); //Reverse path

        return dist[destIndex];
    }

}
