package main;

import java.io.File;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import metro.Percurso;
import metro.Station;
import utils.CalculoPercursos;
import utils.Conectividade;
import utils.FileManager;

public class Main {

    private static final String COORDINATES = "coordinates.csv";
    private static final String CONNECTIONS = "connections.csv";
    private static final String LINES_STATIONS = "lines_and_stations.csv";

    private static boolean validOption = true;
    private static boolean endOption = false;

    private static boolean imported = false;

    public static void main(String[] args) throws Exception {
        do {
            if (!validOption) {
                System.out.println("Insira uma opção válida.");
                System.out.println("");
            }
            menuDisplay();
            Scanner in = new Scanner(System.in);
            try {
                int num = Integer.parseInt(in.nextLine());
                menu(num);
                if (!endOption) {
                    pause();
                }
            } catch (NumberFormatException e) {
                System.out.println("");
                System.out.println("Insira um número.");
                System.out.println("");
            }
        } while (!endOption);
    }

    private static void menuDisplay() {
        String str = "Menu:"
                + "\n1 - Importar ficheiros;"
                + "\n2 - Verificar se o grafo é conexo;"
                + "\n3 - Percurso mínimo entre duas estações em relação ao número de estações;"
                + "\n4 - Percurso mínimo entre duas estações em relação ao tempo;"
                + "\n5 - Percurso mínimo entre duas estações em relação ao número de linhas;"
                + "\n6 - Percurso mínimo entre duas estações em relação ao tempo passando por estações intermédias;"
                + "\n9 - Apresentar o grafo;"
                + "\n0 - Cancelar."
                + "\nOpção: ";
        System.out.print(str);
    }

    private static void menu(int option) throws Exception {
        switch (option) {
            case 1:
                imported = FileManager.readFile(new File(LINES_STATIONS));
                imported = FileManager.readFile(new File(COORDINATES));
                imported = FileManager.readFile(new File(CONNECTIONS));
                if (!imported) {
                    System.out.println("");
                    System.out.println("Os ficheiros não foram importados devido a um erro neles.");
                } else {
                    System.out.println("Os ficheiros foram importados corretamente.");
                }
                validOption = true;
                break;
            case 2:
                if (imported) {
                    boolean worked = Conectividade.isConexo(FileManager.getGraph());
                    if (worked) {
                        System.out.println("O grafo é conexo.");
                    } else {
                        System.out.println("O grafo não é conexo.");
                        System.out.println("Componentes:");
                        System.out.println(Conectividade.getComponents(FileManager.getGraph(), new ArrayList<>((List<Station>) Arrays.asList(FileManager.getGraph().allkeyVerts()))));
                    }
                } else {
                    System.out.println("Os dados ainda não foram importados.");
                }
                validOption = true;
                break;
            case 3:
                if (imported) {
                    System.out.println("");
                    int[] vertices = chooseVertices();
                    System.out.println("");
                    LocalTime instanteInicial = chooseTime();
                    if (vertices[0] != -1 && !instanteInicial.equals(LocalTime.MAX)) {
                        Station origin = FileManager.getGraph().allkeyVerts()[vertices[0]];
                        Station destination = FileManager.getGraph().allkeyVerts()[vertices[1]];
                        System.out.println("");
                        Percurso menorPerc = CalculoPercursos.percursoMaisCurtoEstacoes(FileManager.getGraph(), origin, destination, instanteInicial);
                        System.out.println(menorPerc);
                    } else {
                        System.out.println("Não foram escolhidos nenhuns vértices.");
                        System.out.println("");
                    }
                } else {
                    System.out.println("Os dados ainda não foram importados.");
                }
                validOption = true;
                break;
            case 4:
                if (imported) {
                    System.out.println("");
                    int[] vertices = chooseVertices();
                    System.out.println("");
                    LocalTime instanteInicial = chooseTime();
                    if (vertices[0] != -1 && !instanteInicial.equals(LocalTime.MAX)) {
                        System.out.println("");
                        System.out.println(CalculoPercursos.percursoMaisCurtoTempo(FileManager.getGraph(), FileManager.getGraph().allkeyVerts()[vertices[0]], FileManager.getGraph().allkeyVerts()[vertices[1]], instanteInicial));
                    } else {
                        System.out.println("Não foram escolhidos nenhuns vértices.");
                        System.out.println("");
                    }
                } else {
                    System.out.println("Os dados ainda não foram importados.");
                }
                validOption = true;
                break;
            case 5:
                if (imported) {
                    System.out.println("");
                    int[] vertices = chooseVertices();
                    System.out.println("");
                    LocalTime instanteInicial = chooseTime();
                    if (vertices[0] != -1 && !instanteInicial.equals(LocalTime.MAX)) {
                        System.out.println("");
                        System.out.println(CalculoPercursos.percursoMaisCurtoLinhas(FileManager.getGraph(), FileManager.getGraph().allkeyVerts()[vertices[0]], FileManager.getGraph().allkeyVerts()[vertices[1]], instanteInicial));
                    } else {
                        System.out.println("Não foram escolhidos nenhuns vértices.");
                        System.out.println("");
                    }
                } else {
                    System.out.println("Os dados ainda não foram importados.");
                }
                validOption = true;
                break;
            case 6:
                if (imported) {
                    System.out.println("");
                    int[] vertices = chooseVertices();
                    System.out.println("");
                    List<Station> intermedios = verticesIntermedios();
                    System.out.println("");
                    LocalTime instanteInicial = chooseTime();
                    if (vertices[0] != -1 && !instanteInicial.equals(LocalTime.MAX) && intermedios != null) {
                        System.out.println("");
                        System.out.println(CalculoPercursos.percursoMaisCurtoIntermedios(FileManager.getGraph(), FileManager.getGraph().allkeyVerts()[vertices[0]], FileManager.getGraph().allkeyVerts()[vertices[1]], intermedios, instanteInicial));
                    } else {
                        System.out.println("Não foram escolhidos nenhuns vértices.");
                        System.out.println("");
                    }
                } else {
                    System.out.println("Os dados ainda não foram importados.");
                }
                validOption = true;
                break;
            case 9:
                if (imported) {
                    System.out.println(FileManager.getGraph());
                } else {
                    System.out.println("Os dados ainda não foram importados.");
                }
                validOption = true;
                break;
            case 0:
                validOption = true;
                endOption = true;
                break;
            default:
                validOption = false;
                break;
        }
        System.out.println("");
    }

    private static int[] chooseVertices() {
        System.out.println("");
        int[] newInt = {-1, -1};
        int[] vertices = new int[2];
        String str = "1 - Escolher através dos números do vértice sem a lista;"
                + "\n2 - Mostrar todos os vértices e selecionar os vértices pelos números;"
                + "\n0 - Cancelar.";
        int op = -1;
        boolean called = true;
        do {
            System.out.println(str);
            Scanner in = new Scanner(System.in);
            System.out.print("Opção: ");
            try {
                op = in.nextInt();
                switch (op) {
                    case 1:
                        called = true;
                        break;
                    case 2:
                        int num = 0;
                        for (Object vert : FileManager.getGraph().vertices()) {
                            System.out.println(num + " - " + vert);
                            num++;
                        }
                        called = true;
                        break;
                    case 0:
                        return newInt;
                    default:
                        called = false;
                        System.out.println("");
                        break;
                }
                boolean worked = false;
                if (called) {
                    do {
                        try {
                            Scanner in2 = new Scanner(System.in);
                            int v1 = -1;
                            int v2 = -1;
                            do {
                                System.out.print("Estação de origem: ");
                                v1 = in2.nextInt();
                            } while (v1 > 376 || v1 < 0);
                            vertices[0] = v1;
                            do {
                                System.out.print("Estação de destino: ");
                                v2 = in2.nextInt();
                            } while (v2 > 376 || v2 < 0);
                            vertices[1] = v2;
                            worked = true;
                            return vertices;
                        } catch (NumberFormatException nfe) {
                            System.out.println("Insira um número");
                            worked = false;
                        }
                    } while (!worked);
                }
            } catch (NumberFormatException nfe) {
                return newInt;
            }
        } while (op != 0);
        return newInt;
    }

    private static void pause() {
        Scanner in = new Scanner(System.in);
        System.out.println("");
        String ENTER = "===Digite ENTER para avançar===";
        System.out.format(ENTER + "\n");
        in.nextLine();
    }

    private static LocalTime chooseTime() {
        String str = "1 - Hora atual;"
                + "\n2 - Inserir hora;"
                + "\n3 - Tempo a zeros (00:00:00);"
                + "\n0 - Cancelar";
        int op = -1;
        boolean called = true;
        do {
            System.out.println(str);
            Scanner in = new Scanner(System.in);
            System.out.print("Opção: ");
            try {
                op = in.nextInt();
                switch (op) {
                    case 1:
                        return LocalTime.now();
                    case 2:
                        boolean worked = false;
                        Scanner time = new Scanner(System.in);
                        System.out.print("Hora: ");
                        int hour = time.nextInt();
                        System.out.print("Minuto: ");
                        int minute = time.nextInt();
                        System.out.print("Segundo: ");
                        int second = time.nextInt();
                        LocalTime localTime = LocalTime.MIN;
                        localTime = localTime.withHour(hour);
                        localTime = localTime.withMinute(minute);
                        localTime = localTime.withSecond(second);
                        return localTime;
                    case 3:
                        return LocalTime.MIN;
                    case 0:
                        break;
                    default:
                        called = false;
                        System.out.println("");
                        break;
                }
            } catch (NumberFormatException nfe) {
                System.out.println("erro");
            }
        } while (op != 0);
        return LocalTime.MAX;
    }

    private static List<Station> verticesIntermedios() {
        System.out.println("");
        List<Station> defaultList = null;
        List<Station> vertices = new ArrayList<>();
        String str = "1 - Escolher através dos números do vértice sem a lista;"
                + "\n2 - Mostrar todos os vértices e selecionar os vértices pelos números;"
                + "\n0 - Cancelar.";
        int op = -1;
        boolean called = true;
        do {
            System.out.println(str);
            Scanner in = new Scanner(System.in);
            System.out.print("Opção: ");
            try {
                op = in.nextInt();
                switch (op) {
                    case 1:
                        called = true;
                        break;
                    case 2:
                        int num = 0;
                        for (Object vert : FileManager.getGraph().vertices()) {
                            System.out.println(num + " - " + vert);
                            num++;
                        }
                        called = true;
                        break;
                    case 0:
                        return defaultList;
                    default:
                        called = false;
                        System.out.println("");
                        break;
                }
                boolean worked = false;
                if (called) {
                    do {
                        try {
                            Scanner in2 = new Scanner(System.in);
                            int v1 = -1;
                            boolean finished = false;
                            do {
                                System.out.println("Escreva -1 para terminar;");
                                System.out.print("Estação intermediária " + (vertices.size() + 1) + ": ");
                                v1 = in2.nextInt();
                                if (v1 > -1 && v1 < 376) {
                                    vertices.add(FileManager.getGraph().allkeyVerts()[v1]);
                                } else if (v1 == -1) {
                                    v1 = 0;
                                    finished = true;
                                }
                            } while (v1 > 376 || v1 < 0 || !finished);
                            if (v1 == -1) {
                                return defaultList;
                            }
                            worked = true;
                            return vertices;
                        } catch (NumberFormatException nfe) {
                            System.out.println("Insira um número");
                            worked = false;
                        }
                    } while (!worked);
                }
            } catch (NumberFormatException nfe) {
                return defaultList;
            }
        } while (op != 0);
        return defaultList;
    }
}
